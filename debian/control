Source: python-jsonpointer
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Christopher Hoskin <mans0954@debian.org>
Build-Depends: debhelper-compat (= 9), python-setuptools, python-all, python3-all, python3-setuptools, dh-python
Standards-Version: 3.9.8
Homepage: https://pypi.python.org/pypi/jsonpointer
Vcs-Git: https://salsa.debian.org/python-team/packages/python-jsonpointer.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-jsonpointer

Package: python-jsonpointer
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}, ${shlibs:Depends}, python
Description: library to resolve JSON Pointers (Python 2)
 JSON Pointer (RFC 6901) defines a string syntax for identifying a specific
 value within a JavaScript Object Notation (JSON) document.
 .
 jsonpointer is a library to resolve JSON Pointers according to RFC 6901.
 .
 This is the Python 2 version of the package.

Package: python3-jsonpointer
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, ${shlibs:Depends}, python3
Description: library to resolve JSON Pointers (Python 3)
 JSON Pointer (RFC 6901) defines a string syntax for identifying a specific
 value within a JavaScript Object Notation (JSON) document.
 .
 jsonpointer is a library to resolve JSON Pointers according to RFC 6901.
 .
 This is the Python 3 version of the package.

Package: jsonpointer-bin
Architecture: all
Section: utils
Depends: ${python:Depends}, ${misc:Depends}, ${shlibs:Depends}, python, python-jsonpointer
Description: library to resolve JSON Pointers (executable file)
 JSON Pointer (RFC 6901) defines a string syntax for identifying a specific
 value within a JavaScript Object Notation (JSON) document.
 .
 jsonpointer is a library to resolve JSON Pointers according to RFC 6901.
 .
 This package provides the jsonpointer executable.
